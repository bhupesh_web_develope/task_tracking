## Task tracking web page- Laravel | ReactJs | MySQL


### TECHNOLOGY

* **Laravel**: Laravel 8.12 - PHP 7.3 or higher user for Backend.
* **React Js**: V16.2 use for Frontend.
* **MySQL**: Database.

### Installation and Configuration

**1. Enter git clone and the repository URL at your command line::** 
~~~
git clone https://bhupesh_web_develope@bitbucket.org/bhupesh_web_develope/task_tracking.git
~~~

**2. Goto task_tracking directory and composer update:** 
~~~
composer install

npm install
~~~

**3. Copy `env.example` to `.env` and generate app key:** 
~~~
cp .env.example .env

php artisan key:generate
~~~

**3.1. You need to set your `APP_NAME` and `APP_URL` from `.env` file** 

**4. Create a database `task_tracking` or if you want to change database name just go in .env file and change value for `DB_DATABASE` key:**

**5. Now, Run migration to create a table in your database:** 
~~~
php artisan migration
~~~

**6. Finally, Start your server:**
~~~
php artisan serve
~~~
