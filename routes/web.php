<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\SubTaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('task',TaskController::class);
Route::post('task/complete/{task}', [TaskController::class, 'complete'])->name('task.complete');
Route::post('task/clone/{task}', [TaskController::class, 'clone'])->name('task.clone');
Route::post('/task/{id}/sub-task', [SubTaskController::class,'store'])->name('sub_task.store');
Route::post('/task/{id}/sub-task/stop', [SubTaskController::class, 'stop'])->name('sub_task.stope');
