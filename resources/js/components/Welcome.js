import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Nav from './Nav';
import TaskTracking from "./TaskTracking";
import './style.css';

function Welcome() {

    return (
        <BrowserRouter>
            <div className="container-fluid">

                <Nav/>

                <TaskTracking/>
            </div>
        </BrowserRouter>
    );
}

export default Welcome;

if (document.getElementById('task_tracking')) {
    ReactDOM.render(<Welcome/>, document.getElementById('task_tracking'));
}
