import React from 'react';

class TaskList extends React.Component {
    render() {
        const {task} = this.props;

        let bgColor = "";
        if (task.status === 2) {
            bgColor = 'table-success'
        }
        if (task.status === 3) {
            bgColor = 'table-danger'
        }

        return (
            <tr className={bgColor}>
                <th scope="row">{task.id}</th>
                <td>{task.name}</td>
                <td>{task.date_time}</td>
                <td>{task.task_name}</td>
                <td>
                    <button type="button"
                            onClick={() => this.props.handleTask('edit', task.id)}
                            className="btn btn-outline-primary">Edit
                    </button>

                    {(task.status !== 2 && task.status !== 3) ?
                        <button type="button"
                                onClick={() => this.props.handleTask('complete', task.id)}
                                className="btn btn-outline-success ml-2">Complete</button>
                        : ''
                    }

                    {(task.status !== 3) ?
                        <button onClick={() => this.props.handleTask('delete', task.id)}
                                type="button"
                                className="btn btn-outline-danger ml-2">Delete</button>
                        : ''
                    }

                    {/*<button type="button"*/}
                    {/*        onClick={() => this.props.handleTask('clone', task.id)}*/}
                    {/*        className="btn btn-outline-secondary">Clone*/}
                    {/*</button>*/}
                </td>
            </tr>
        )
    }
}

export default TaskList;
