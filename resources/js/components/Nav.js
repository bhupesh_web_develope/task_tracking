import React from 'react';
import { Link } from 'react-router-dom'

const Nav = () => {
    return(
        <nav className="navbar navbar-dark bg-primary">
            <Link className='navbar-brand' to='/'><span className="navbar-brand mb-0 h1">Task Tracking</span></Link>
        </nav>
    )
}

export default Nav;
