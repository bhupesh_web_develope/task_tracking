import React from 'react';
import Form from './Form';
import TaskList from "./TaskList";
import axios from "axios";
import {toast, ToastContainer} from "react-toastify";

class TaskTracking extends React.Component {

    state = {
        tasks: [],
        loading: true,
        formSubmitted: false,
        editableId: ''
    }

    getTaskList = async () => {
        const response = await axios.get('/task').then(response => {
            if (response.status === 200) {
                //toast.success(response.data.message);

                this.setState({
                    tasks: response.data.data,
                    loading: false,
                    //editableId: ''
                })
            }
        }).catch(errors => {
            //toast.error(errors.response.data.message);
        });
    }

    componentDidMount() {
        this.getTaskList();
    }

    formSubmitted = (status) => {
        if (status === true) {
            this.setState({editableId: ''})
            this.getTaskList();
        }
    }

    handleTask = async (type, id) => {
        let url = "";

        if (type === "delete") {
            url = "/task/" + id;
            const response = await axios.delete(url).then(response => {
                if (response.status === 200) {
                    toast.success(response.data.message);

                    this.getTaskList();
                }
            }).catch(errors => {
                toast.error(errors.response.data.message);
            });
        }

        if (type === "complete" || type === 'clone') {
            url = "/task/" + type + "/" + id;

            const response = await axios.post(url).then(response => {
                if (response.status === 200) {
                    toast.success(response.data.message);

                    this.getTaskList();
                }
            }).catch(errors => {
                toast.error(errors.response.data.message);
            });
        }

        if( type === 'edit' ) {
            this.setState({ editableId: id })
        }
    }

    render() {
        return (
            <React.Fragment>
                <ToastContainer/>

                <div className="row mt-5">
                    <div className="col-md-12">

                        <Form formSubmitted={this.formSubmitted} handleTask={this.handleTask} editId={this.state.editableId}/>

                    </div>
                </div>

                <div className="row mt-5">
                    <div className="col-md-12">
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Date & Time</th>
                                <th scope="col">Task Name</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            {
                                this.state.tasks.length > 0 ?
                                this.state.tasks.map((task) =>
                                    <TaskList key={task.id} task={task} handleTask={this.handleTask}/>
                                ) :
                                    <tr>
                                        <td colSpan={5}>No task found!</td>
                                    </tr>
                            }

                            </tbody>
                        </table>

                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default TaskTracking;

