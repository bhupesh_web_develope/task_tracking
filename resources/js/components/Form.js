import React, {useState} from 'react';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import Modal from "react-modal";
import _ from "lodash";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class Form extends React.Component {
    state = {
        name: "",
        date_time: "",
        task_name: "",
        sub_task_name: "",
        id: this.props.editId,
        timer_label: 'Start Time',
        is_timer_start: true,
        isOpen: false
    }

    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    saveTask = async (e) => {
        e.preventDefault();

        document.getElementById("nameHelp").innerHTML = "";
        document.getElementById("dateTimeHelp").innerHTML = "";
        document.getElementById("taskNameHelp").innerHTML = "";

        let currentDate = this.state.date_time;
        let datetime = currentDate.getDate() + "-"
            + (currentDate.getMonth()+1)  + "-"
            + currentDate.getFullYear() + " "
            + currentDate.getHours() + ":"
            + currentDate.getMinutes() + ":"
            + currentDate.getSeconds();

        const data = {
            name: this.state.name,
            date_time: datetime,
            task_name: this.state.task_name
        };

        if (this.props.editId) {
            const response = await axios.put('/task/' + this.props.editId, data).then(response => {
                if (response.status === 200) {
                    toast.success(response.data.message);

                    this.setState({
                        name: "",
                        date_time: "",
                        task_name: ""
                    });

                    this.props.formSubmitted(true);
                }
            }).catch(errors => {
                this.errorResponse(errors);
            });

        } else {
            const response = await axios.post('/task', data).then(response => {
                if (response.status === 200) {
                    toast.success(response.data.message);

                    this.setState({
                        name: "",
                        date_time: "",
                        task_name: ""
                    });

                    this.props.formSubmitted(true);
                }
            }).catch(errors => {
                this.errorResponse(errors);
            });
        }
    }

    errorResponse = (errors) => {
        toast.error(errors.response.data.message);

        if (errors.response.data.errors.name !== undefined) {
            document.getElementById("nameHelp").innerHTML = errors.response.data.errors.name[0];
        }

        if (errors.response.data.errors.date_time !== undefined) {
            document.getElementById("dateTimeHelp").innerHTML = errors.response.data.errors.date_time[0];
        }

        if (errors.response.data.errors.task_name !== undefined) {
            document.getElementById("taskNameHelp").innerHTML = errors.response.data.errors.task_name[0];
        }
    }

    editTask = async () => {
        const response = await axios.get('/task/' + this.props.editId + '/edit').then(response => {
            if (response.status === 200) {
                toast.success(response.data.message);
                if (_.isEmpty(response.data.data.sub_task)) {
                    this.setState({is_timer_start: true, timer_label: 'Start Time'})
                } else {
                    this.setState({is_timer_start: false, timer_label: 'End Time'})
                }

                this.setState({
                    name: response.data.data.name,
                    date_time: new Date(response.data.data.date_time),
                    task_name: response.data.data.task_name
                })
            }
        }).catch(errors => {
            toast.error(errors.response.data.message);
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.editId !== this.props.editId && this.props.editId !== '') {
            this.editTask();
        }
    }

    startTimer = async (e) => {
        const data = {'name': this.state.sub_task_name};
        const response = await axios.post('/task/' + this.props.editId + '/sub-task', data).then(response => {
            if (response.status === 200) {
                this.setState({is_timer_start: false, timer_label: 'End Time', sub_task_name: ''})

                toast.success(response.data.message);
            }
        }).catch(errors => {
            toast.error(errors.response.data.message);
        });
    }

    stopTimer = async (e) => {
        const response = await axios.post('/task/' + this.props.editId + '/sub-task/stop').then(response => {
            if (response.status === 200) {
                this.setState({is_timer_start: true, timer_label: 'Start Time', sub_task_name: ''})

                toast.success(response.data.message);
            }
        }).catch(errors => {
            toast.error(errors.response.data.message);
        });
    }

    handleTimer = () => {
        if (this.state.is_timer_start === true) {
            if (this.state.isOpen === false) {
                this.setState({isOpen: true})
            } else {
                this.startTimer()
            }
        }

        if (this.state.is_timer_start === false) {
            this.stopTimer()
        }
    }

    closeModal = () => {
        document.getElementById("subTaskNameHelp").innerHTML = "";
        if (this.state.sub_task_name === '') {
            document.getElementById("subTaskNameHelp").innerHTML = 'This field is required';
        } else {
            this.setState({isOpen: false})
            this.handleTimer();
        }
    }

    filterPassedTime(time) {
        const currentDate = new Date();
        const selectedDate = new Date(time);

        return currentDate.getTime() < selectedDate.getTime();
    }

    handleDateInput = (date) => {
        this.setState({date_time: date})
    }

    render() {

        return (
            <React.Fragment>
                <ToastContainer/>

                <form className="container" onSubmit={this.saveTask}>
                    <div className="form-row">
                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="name">Name</label>
                            <input type="text"
                                   value={this.state.name}
                                   onChange={this.handleInput}
                                   className="form-control"
                                   id="name"
                                   name="name"
                                   aria-describedby="nameHelp"
                                   placeholder="Enter name"/>
                            <small id="nameHelp" className="form-text text-danger">&nbsp;</small>
                        </div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="date_time">Date & Time</label>
                            <DatePicker
                                autoComplete={"off"}
                                value={this.state.date_time}
                                className="form-control"
                                id="date_time"
                                name="date_time"
                                aria-describedby="dateTimeHelp"
                                placeholderText="Enter date & time"
                                //isClearable
                                minDate={new Date()}
                                selected={this.state.date_time}
                                //onChange={date => this.handleDateInput(date)}
                                onChange={date => this.handleDateInput(date)}
                                showTimeSelect
                                filterTime={this.filterPassedTime}
                                dateFormat="yyyy-MM-dd HH:mm:ss"
                                //peekNextMonth
                                //showMonthDropdown
                                //showYearDropdown
                                //dropdownMode="select"
                            />
                            <small id="dateTimeHelp" className="form-text text-danger">&nbsp;</small>
                            {/*<input type="text"da*/}
                            {/*       value={this.state.date_time}*/}
                            {/*       onChange={this.handleInput}*/}
                            {/*       className="form-control"*/}
                            {/*       id="date_time"*/}
                            {/*       name="date_time"*/}
                            {/*       aria-describedby="dateTimeHelp"*/}
                            {/*       placeholder="Enter date & time"/>*/}
                        </div>

                        <div className="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label htmlFor="task_name">Task Name</label>
                            <input type="text"
                                   value={this.state.task_name}
                                   onChange={this.handleInput}
                                   className="form-control"
                                   id="task_name"
                                   name="task_name"
                                   aria-describedby="taskNameHelp"
                                   placeholder="Enter task name"/>
                            <small id="taskNameHelp" className="form-text text-danger">&nbsp;</small>
                        </div>

                    </div>
                    <div className="col text-center">
                        <button type="submit"
                                className="btn btn-primary">
                            {(this.props.editId && this.props.editId !== '') ? 'Update' : 'Submit'}
                        </button>

                        {(this.props.editId && this.props.editId !== '') ?
                            <button type="button"
                                    onClick={() => this.props.handleTask('clone', this.props.editId)}
                                    className="btn btn-warning ml-2">Clone
                            </button>
                            : ''}

                        {(this.props.editId && this.props.editId !== '') ?
                            <button type="button"
                                    onClick={this.handleTimer}
                                    className={`btn ml-2 ${this.state.is_timer_start ? "btn-dark" : "btn-danger"}`}>
                                {this.state.timer_label}
                            </button>
                            : ''}
                    </div>
                </form>

                <Modal
                    isOpen={this.state.isOpen}
                    onRequestClose={this.closeModal}
                    contentLabel="sub-tasks & Start / Stop Time"
                    className="mymodal"
                    overlayClassName="myoverlay"
                >
                    <h2>sub-tasks & Start / Stop Time</h2>
                    <div className="row mt-2">
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label htmlFor="sub_task_name">Task Name</label>
                            <input type="text"
                                   value={this.state.sub_task_name}
                                   onChange={this.handleInput}
                                   className="form-control"
                                   id="sub_task_name"
                                   name="sub_task_name"
                                   aria-describedby="subTaskNameHelp"
                                   placeholder="Enter sub task name"/>
                            <small id="subTaskNameHelp" className="form-text text-danger">&nbsp;</small>
                        </div>
                    </div>
                    <button className="btn btn-success"
                            onClick={this.closeModal}>Save Sub Task
                    </button>
                </Modal>
            </React.Fragment>
        )
    }
}

export default Form;
