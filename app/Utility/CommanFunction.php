<?php

if (!function_exists('datetime_timestamp')) {
    /**
     * @param $datetime
     * @return false|string
     */
    function datetime_timestamp($datetime)
    {
        return date('Y-m-d H:i:s', strtotime($datetime));
    }
}
