<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SubTask extends Model
{
    use HasFactory;

    /**
     * {@inheritDoc}
     */
    protected $fillable = ['name', 'start_date_time', 'end_date_time', 'task_id'];

    /**
     * {@inheritDoc}
     */
    protected $dates = ['start_date_time', 'end_date_time'];

    /**
     * Get the sub task that owns the task.
     *
     * @return BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    /**
     * Get the running timers
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRunning($query)
    {
        return $query->whereNull('end_date_time');
    }
}
