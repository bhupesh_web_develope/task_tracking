<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Task extends Model
{
    use HasFactory;

    const STATUS_NEW = 1;
    const STATUS_COMPLETE = 2;
    const STATUS_DELETE = 3;


    /**
     * {@inheritDoc}
     */
    protected $fillable = ['name', 'date_time', 'task_name', 'status'];

    /**
     * {@inheritDoc}
     */
    protected $with = ['subTask'];

    /**
     * Set the task date time.
     *
     * @param string $value
     * @return void
     */
    public function setDateTimeAttribute(string $value)
    {
        $this->attributes['date_time'] = datetime_timestamp($value);
    }

    /**
     * Get the task has a sub tasks.
     *
     * @return HasMany
     */
    public function subTask()
    {
        return $this->hasMany(SubTask::class)->running()->latest();
    }

    /**
     * Get the task has a sub tasks.
     *
     * @return HasMany
     */
    public function subTasks()
    {
        return $this->hasMany(SubTask::class);
    }
}
