<?php

namespace App\Http\Controllers;

use App\Models\SubTask;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class SubTaskController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function store(Request $request, int $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => true,
                'errors' => $validator->messages(),
                'message' => 'Sub Task form validation error.'
            ], Response::HTTP_BAD_REQUEST);
        }

        $subTask = Task::findOrFail($id)
            ->subTask()
            ->save(new SubTask([
                'name' => $request->name,
                'task_id' => $id,
                'start_date_time' => new Carbon(),
            ]));

        return response()->json([
            'status' => true,
            'data' => $subTask,
            'message' => 'Sub Task saved successfully.'
        ], Response::HTTP_OK);
    }

    public function stop(Request $request, int $id)
    {
        if ($subTask = SubTask::running()->where('task_id',$id)->first()) {
            $subTask->update(['end_date_time' => new Carbon]);
        }

        return response()->json([
            'status' => true,
            'data' => $subTask,
            'message' => 'Sub Task saved successfully.'
        ], Response::HTTP_OK);
    }
}
