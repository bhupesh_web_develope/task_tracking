<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $tasks = Task::all();

        return response()->json([
            'status' => true,
            'data' => $tasks,
            'message' => 'Get all tasks records successfully.'
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'date_time' => 'required',
            'task_name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => true,
                'errors' => $validator->messages(),
                'message' => 'Task form validation error.'
            ], Response::HTTP_BAD_REQUEST);
        }

        Task::create([
            'name' => $request->name,
            'date_time' => $request->date_time,
            'task_name' => $request->task_name,
            'status' => Task::STATUS_NEW
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Task saved successfully.'
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Task $task
     * @return void
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Task $task
     * @return JsonResponse
     */
    public function edit(Task $task)
    {
        return response()->json([
            'status' => true,
            'data' => $task,
            'message' => 'Get task record successfully.'
        ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Task $task
     * @return JsonResponse
     */
    public function update(Request $request, Task $task)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'date_time' => 'required',
            'task_name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => true,
                'errors' => $validator->messages(),
                'message' => 'Task form validation error.'
            ], Response::HTTP_BAD_REQUEST);
        }

        $task->name = $request->name;
        $task->date_time = $request->date_time;
        $task->task_name = $request->task_name;
        $task->save();

        return response()->json([
            'status' => true,
            'message' => 'Task updated successfully.'
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return JsonResponse
     */
    public function destroy(Task $task)
    {
        $task->status = Task::STATUS_DELETE;
        $task->save();

        return response()->json([
            'status' => true,
            'message' => 'Task deleted successfully.'
        ], Response::HTTP_OK);
    }

    /**
     * Complete the specified resource from storage.
     *
     * @param Task $task
     * @return JsonResponse
     */
    public function complete(Task $task)
    {
        $task->status = Task::STATUS_COMPLETE;
        $task->save();

        return response()->json([
            'status' => true,
            'message' => 'Task completed successfully.'
        ], Response::HTTP_OK);
    }

    /**
     * Clone the specified resource from storage.
     *
     * @param Task $task
     * @return JsonResponse
     */
    public function clone(Task $task)
    {
        DB::beginTransaction();
        try {
            $newTask = $task->replicate();
            $newTask->save();

            if ($task->subTask->count() > 0) {
                foreach ($task->subTasks as $subTask) {
                    $netSubTask = $subTask->replicate();
                    $netSubTask->task_id = $newTask->id;

                    $newTask->subTasks()->save($netSubTask);
                }
            }

            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'Task clone and added successfully.'
            ], Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => fasle,
                'message' => 'Task clone and added successfully.'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
